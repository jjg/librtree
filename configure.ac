AC_INIT([librtree],
        [1.3.2],
        [j.j.green@gmx.co.uk],
        [librtree],
        [https://gitlab.com/jjg/librtree])

AC_CONFIG_MACRO_DIR([config/m4])
AC_CONFIG_AUX_DIR([config])

m4_include([config/aclocal.m4])

LT_INIT([disable-shared])

opt_enable_json=yes
opt_enable_bsrt=yes
opt_enable_simd=no

AC_ARG_ENABLE(json,
  AS_HELP_STRING([--enable-json],
        [JSON support (default is yes)]),
  [opt_enable_json=$enableval])

AC_ARG_ENABLE(bsrt,
  AS_HELP_STRING([--enable-bsrt],
        [Binary serialisation support (default is yes)]),
  [opt_enable_bsrt=$enableval])

AC_ARG_ENABLE(simd,
  AS_HELP_STRING([--enable-simd],
        [use SIMD intrinsics (default is no)]),
  [opt_enable_simd=$enableval])

opt_enable_acceptance_tests=no
opt_enable_unit_tests=no
opt_enable_memory_tests=no
opt_enable_lint_tests=no

opt_enable_profile=no
opt_enable_coverage=no

AC_ARG_ENABLE(unit-tests,
  AS_HELP_STRING([--enable-unit-tests],
        [unit tests (default is no)]),
  [opt_enable_unit_tests=$enableval])

AC_ARG_ENABLE(memory-tests,
  AS_HELP_STRING([--enable-memory-tests],
        [memory tests (default is no)]),
  [opt_enable_memory_tests=$enableval])

AC_ARG_ENABLE(lint-tests,
  AS_HELP_STRING([--enable-lint-tests],
        [lint tests (default is no)]),
  [opt_enable_lint_tests=$enableval])

AC_ARG_ENABLE(acceptance-tests,
  AS_HELP_STRING([--enable-acceptance-tests],
        [acceptance tests (default is no)]),
  [opt_enable_acceptance_tests=$enableval])

AC_ARG_ENABLE(profile,
  AS_HELP_STRING([--enable-profile],
        [instrument for profiling (default is no)]),
  [opt_enable_profile=$enableval])

AC_ARG_ENABLE(coverage,
  AS_HELP_STRING([--enable-coverage],
        [instrument for coverage (default is no)]),
  [opt_enable_coverage=$enableval])

if test "$enable_static" = 'no' ; then
  if test "$opt_enable_unit_tests" = 'yes' ; then
    AC_MSG_ERROR([unit tests need --enable-static])
  fi
  if test "$opt_enable_memory_tests" = 'yes' ; then
    AC_MSG_ERROR([memory tests need --enable-static])
  fi
  if test "$opt_enable_profile" = 'yes' ; then
    AC_MSG_ERROR([profiling need --enable-static])
  fi
fi

AC_SUBST(ENABLE_LINT_TESTS, $opt_enable_lint_tests)
AC_SUBST(ENABLE_UNIT_TESTS, $opt_enable_unit_tests)
AC_SUBST(ENABLE_MEMORY_TESTS, $opt_enable_memory_tests)
AC_SUBST(ENABLE_ACCEPTANCE_TESTS, $opt_enable_acceptance_tests)

AC_PROG_CC([gcc clang])
AS_IF([test "$GCC" = 'yes'], [CFLAGS="$CFLAGS -Wall -Wextra"])

AC_PATH_PROG([XDGOPEN], [xdg-open])

AC_PROG_MAKE_SET
AC_PROG_INSTALL

AC_CHECK_HEADERS([unistd.h])
AC_CHECK_HEADERS([dirent.h])
AC_CHECK_HEADERS([stdalign.h])
AC_CHECK_HEADERS([tgmath.h])
AC_CHECK_HEADERS([features.h])

AC_CHECK_FUNCS([sysconf])
AC_CHECK_FUNCS([getpagesize])
AC_CHECK_DECLS([alignof], [], [], [[#include <stdalign.h>]])

AC_CHECK_LIB(m, pow)

AX_GCC_BUILTIN(__builtin_ctzl)

if test "$opt_enable_json" = 'yes' ; then
  AC_CHECK_HEADERS([jansson.h])
  AC_CHECK_LIB(jansson, json_pack, [], AC_MSG_ERROR(libjansson not found))
  AC_DEFINE(WITH_JSON, 1, [Define to 1 for JSON support])
fi

if test "$opt_enable_bsrt" = 'yes' ; then
  AC_CHECK_HEADERS([endian.h sys/endian.h libkern/OSByteOrder.h winsock2.h])
  AX_C_FLOAT_WORDS_BIGENDIAN()
  AC_CHECK_DECLS([htole16, htole32, htole64])
  AC_CHECK_DECLS([le16toh, le32toh, le64toh])
  AC_DEFINE(WITH_BSRT, 1, [Define to 1 for BSRT support])
fi

if test "$opt_enable_unit_tests" = 'yes' ; then
  AC_CHECK_FUNCS([unlink])
  AC_CHECK_FUNCS([popen])
  AC_CHECK_LIB(
    [cunit],
    [CU_add_test],
    [
      AC_MSG_CHECKING([CUnit version])
      AC_COMPILE_IFELSE([
        AC_LANG_SOURCE([[
          #include <CUnit/CUnit.h>
          int main(void) {
            CU_SuiteInfo suite;
            suite.pSetUpFunc = NULL;
            return 0; }
          ]])
        ],
        [
          AC_MSG_RESULT([2.1-3 or later])
          cunit213=1
        ],
        [
          AC_MSG_RESULT([2.1-2 or earlier])
          cunit213=0
        ]
      )
      AC_DEFINE_UNQUOTED(CUNIT_213,
        $cunit213,
        [Define if CUnit version 2.1-3 or later]
      )
    ],
    AC_MSG_ERROR(libcunit not found)
  )
  AC_PATH_PROG([GS], [gs], [no])
  if test "$ac_cv_path_GS" != 'no' ; then
    AC_DEFINE_UNQUOTED([GS_PATH], ["$ac_cv_path_GS"],
    [Define to ghostscript path if found])
  fi
fi

if test "$opt_enable_memory_tests" = 'yes' ; then
  AC_PATH_PROG([VALGRIND], [valgrind], [no])
  AS_IF([test "$VALGRIND" = 'no'], AC_MSG_ERROR([valgrind not found]))
fi

if test "$opt_enable_lint_tests" = 'yes' ; then
  AC_PATH_PROG([CPPCHECK], [cppcheck], [no])
  AS_IF([test "$CPPCHECK" = 'no'], AC_MSG_ERROR([cppcheck not found]))
fi

if test "$opt_enable_profile" = 'yes' ; then
  if test "$GCC" = 'yes' ; then
    CFLAGS="$CFLAGS -pg"
    LDFLAGS="$LDFLAGS -pg"
  else
    AC_MSG_ERROR([profiling only works with gcc, sorry])
  fi
  AC_PATH_PROG([GPROF], [gprof], [no])
  AS_IF([test "$GPROF" = 'no'], AC_MSG_ERROR([gprof not found]))
fi

if test "$opt_enable_coverage" = 'yes' ; then
  if test "$GCC" = 'yes' ; then
    CFLAGS="$CFLAGS --coverage -fprofile-abs-path"
    LDFLAGS="$LDFLAGS --coverage -fprofile-abs-path"
  else
    AC_MSG_ERROR([profiling only works with gcc, sorry])
  fi
  AC_PATH_PROG([GCOVR], [gcovr], [no])
  AS_IF([test "$GCOVR" = 'no'], AC_MSG_ERROR([gcovr not found]))
fi

if test "$opt_enable_simd" = 'yes' ; then
  AX_EXT
  AC_CHECK_HEADERS([xmmintrin.h])
  AC_CHECK_HEADERS([immintrin.h])
fi

AC_PATH_PROGS(XSLTP, [xsltproc])
AC_PATH_PROGS(GGO, [gengetopt])
AC_PATH_PROGS(PYTHON, [python3 python])
AC_PATH_PROGS(RUBY, [ruby])
AX_CHECK_DOCBOOK_XSLT()

if test -z "$ac_cv_path_PYTHON" ; then
  AC_SUBST(ENABLE_GMPY2, [no])
else
  AX_PYTHON_MODULE(gmpy2)
  AC_SUBST(ENABLE_GMPY2, $HAVE_PYMOD_GMPY2)
fi

if test -z "$ac_cv_path_RUBY" ; then
  AC_SUBST(ENABLE_STYLE_SCRIPTS, [no])
else
  AC_SUBST(ENABLE_STYLE_SCRIPTS, [yes])
fi

if test -z "$ac_cv_path_GGO" ; then
  AC_SUBST(ENABLE_GGO, [no])
else
  AC_SUBST(ENABLE_GGO, [yes])
fi

if test -z "$XSLTPROC" ; then
  AC_SUBST(ENABLE_DOCBOOK, [no])
else
  AC_SUBST(XSLTP, $XSLTPROC)
  AC_SUBST(ENABLE_DOCBOOK, $HAVE_DOCBOOK_XSLT)
fi

AX_CHECK_COMPILE_FLAG([-Wno-unused-but-set-variable],
  AC_SUBST(NUBSV_FLAG, [-Wno-unused-but-set-variable]),
  AC_SUBST(NUBSV_FLAG, []),
  [-Werror])

AC_DEFINE(VERSION, PACKAGE_VERSION, [Version of package])

dnl | The rtree_id_t type (rectangle id)

AC_ARG_VAR(RTREE_ID_TYPE, [the integer type of a rectangle id])
if test -z "$RTREE_ID_TYPE" ; then
  AC_CHECK_SIZEOF([void *])
  AC_TYPE_UINT64_T
  AC_TYPE_UINT32_T
  AC_TYPE_UINT16_T
  case $ac_cv_sizeof_void_p in
    4)
      RTREE_ID_TYPE=uint32_t
      ;;
    8)
      RTREE_ID_TYPE=uint64_t
      ;;
    *)
      AC_MSG_ERROR([strange pointer size, set RTREE_ID_TYPE])
      ;;
  esac
fi
AC_CHECK_SIZEOF(
  [rtree_id_t], [],
  [
    AC_INCLUDES_DEFAULT
    typedef $RTREE_ID_TYPE rtree_id_t;
  ]
)

dnl | the rtree_coord_t type (rectangle coordinate)

AC_ARG_VAR(RTREE_COORD_TYPE, [the float type of rectangle coordinates])
AS_IF([test -z "$RTREE_COORD_TYPE"], [RTREE_COORD_TYPE=float])
AC_CHECK_SIZEOF(
  [rtree_coord_t], [],
  [
    AC_INCLUDES_DEFAULT
    typedef $RTREE_COORD_TYPE rtree_coord_t;
  ]
)

AC_MSG_NOTICE([------------------------])
AC_MSG_NOTICE([static library: $enable_static])
AC_MSG_NOTICE([shared library: $enable_shared])
AC_MSG_NOTICE([rtree_id t: $RTREE_ID_TYPE])
AC_MSG_NOTICE([rtree_coord_t: $RTREE_COORD_TYPE])
AC_MSG_NOTICE([------------------------])

AC_CONFIG_HEADERS([src/librtree/config.h])
AC_CONFIG_FILES([VERSION:config/VERSION.in])
AC_CONFIG_FILES([src/Common.mk])
AC_CONFIG_FILES([src/librtree/rtree/types.h])
AC_CONFIG_FILES([src/librtree/package.c])
AC_CONFIG_FILES([src/test/accept/config.bash])

AC_OUTPUT
