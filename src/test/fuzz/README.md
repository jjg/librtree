Fuzz-testing
------------

Contains some scripts to run [fuzz-testing][1] against the library.
We use the [AFL][2] in the first instance, but might support other
fuzzers later.

The general idea is to test code which reads input, mutating the input
to generate code paths which generate error conditions (segmentation
faults and so on).  So typically a fuzzer will _instrument_ the code
so that it can see code paths, and how modifying the input changes those
code paths.  It will also generally need a set of _seed inputs_ from
which to generate "nearby" fuzzed inputs.

To run fuzzing against the library, first install [AFL][2], on Debian
based systems this is

    sudo apt-get install afl++

Next, one needs to adjust some (Linux) kernel parameters dealing with
how the kernel handles segmentation faults (core-dumps and so on), this
is scripted with

    sudo bin/kernel-setup

You would want to inspect that script before running it of course.

Next, configure and build the library with the AFL compiler wrapper

    bin/build

one can pass the usual `configure` options via environmental variable,
so to build a "double" version

    RTREE_COORD_TYPE=double bin/build

Then copy some seed files into `var/seed`, these should ideally be small
and valid examples of input.  To run the fuzzing

    bin/fuzz-bsrt

which generates output in `var/output`.  The files in the `crashes`
subdirectory are inputs which cause the target program (in this case,
`rtree-eps`) to abort, typically a double-free or segmentation fault.
So these can then become fixtures in the unit-test suite and one fixes
the code so they don't bloody crash.

[1]: https://en.wikipedia.org/wiki/Fuzzing
[2]: https://github.com/google/AFL
