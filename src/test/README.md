Tests
=====

Suites for

- acceptance tests (using Bats) in [accept](accept)
- unit testing (using CUnit) in [unit](unit)
- memory testing (using valgrind) in [memory](memory)

There are numerous fixtures in [fixture](ficture) and a small C library
to access those in uniform manner in [libfixture](libfixture).
