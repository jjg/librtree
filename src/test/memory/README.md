Memory tests
------------

Small standalone programs which exercise a part of the functionality,
reading and writing files, building, searching ... these are run under
`valgrind` with error reports written in XML format in the `tmp`
subdirectory.
