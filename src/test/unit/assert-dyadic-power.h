/*
  assert-dyadic-power.h
  Copyright (c) J.J. Green 2023
*/

#ifndef ASSERT_DYADIC_POWER_H
#define ASSERT_DYADIC_POWER_H

#include <CUnit/CUnit.h>

#include <stdbool.h>
#include <stdlib.h>

#define CU_ASSERT_DYADIC_POWER(n) CU_ASSERT_TRUE(dyadic_power(n))

bool dyadic_power(size_t);

#endif
