#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-error.h"

#include <rtree/error.h>

CU_TestInfo tests_error[] = {
  {"messages are non-null", test_strerror_not_null},
  CU_TEST_INFO_NULL
};

void test_strerror_not_null(void)
{
  int errs[] = {
    RTREE_OK,
    RTREE_ERR_INVAL,
    RTREE_ERR_DOM,
    RTREE_ERR_NOMEM,
    RTREE_ERR_CSVPARSE,
    RTREE_ERR_NOCSV,
    RTREE_ERR_JANSSON,
    RTREE_ERR_NOJSON,
    RTREE_ERR_NOBSRT,
    RTREE_ERR_GETBRANCH,
    RTREE_ERR_GETCHILD,
    RTREE_ERR_NODECLONE,
    RTREE_ERR_PICKBRANCH,
    RTREE_ERR_ADDRECT,
    RTREE_ERR_NOSUCHSPLIT,
    RTREE_ERR_DIMS,
    RTREE_ERR_EMPTY,
    RTREE_ERR_BUFFER,
    RTREE_ERR_POSTSCRIPT,
    RTREE_ERR_USER,
    RTREE_ERR_FWRITE,
    RTREE_ERR_SPLIT
  };
  size_t n = sizeof(errs) / sizeof(int);

  for (size_t i = 0 ; i < n ; i++)
    {
      int err = errs[i];
      const char *msg = strerror_rtree(err);
      CU_ASSERT_PTR_NOT_NULL(msg);
    }
}
