#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <CUnit/CUnit.h>
#include "assert-ps-bbox.h"

#include <stdlib.h>

#ifdef GS_PATH

/* read gs bbox output, extract high-resolution bbox */

static int read_hrbbox(FILE *st, double hrbbox[static 4])
{
  size_t count =
    fscanf(st, "%%%%HiResBoundingBox: %lf %lf %lf %lf",
           hrbbox, hrbbox + 1, hrbbox + 2, hrbbox + 3);

  return (count == 4) ? 0 : 1;
}

/*
  Open a read-pipe for the gs bbox of the path, contrary to the
  Debian documentation, this is sent to stderr, so we need to
  redirect that to stdout, then get the final line of that text.
  Note that tail is POSIX, and we're already using popen(3) so
  we're already restricted to POSIX.
*/

#define CMD_LEN 1024

static int get_hrbbox(const char *path, double hrbbox[static 4])
{
  char cmd[CMD_LEN];
  int written =
    snprintf(cmd, CMD_LEN,
             "'%s' -q -sDEVICE=bbox -dNOPAUSE -dBATCH '%s' 2>&1 | tail -n1",
             GS_PATH, path);
  if (written >= CMD_LEN)
    {
      CU_FAIL("failed to write command");
      return 1;
    }

#ifdef HAVE_POPEN

  FILE *st;
  int err = 1;

  if ((st = popen(cmd, "r")) != NULL)
    {
      err = read_hrbbox(st, hrbbox);
      pclose(st);
    }
  else
    CU_FAIL("failed to open gs");

  return err;

#else

  CU_FAIL("no popen(5)");
  return 1;

#endif
}

void assert_ps_bbox(const char *path, const double bbox[static 4], double eps)
{
  double hrbbox[4];

  if (get_hrbbox(path, hrbbox) == 0)
    {
      for (size_t i = 0 ; i < 4 ; i++)
        CU_ASSERT_DOUBLE_EQUAL(bbox[i], hrbbox[i], eps);
    }
  else
    CU_FAIL("could not get high-resolution bounding box");
}

#else

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
void assert_ps_bbox(const char *path, const double bbox[static 4], double eps)
{
  CU_FAIL("no gs(1)");
}
#pragma GCC diagnostic pop

#endif
