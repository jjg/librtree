#include <CUnit/CUnit.h>

extern CU_TestInfo tests_state[];

void test_state_new_valid(void);
void test_state_new_invalid(void);
void test_state_new_dims_too_large(void);
void test_state_dims(void);
void test_state_page_size(void);
void test_state_node_size(void);
void test_state_branch_size(void);
void test_state_branching_factor(void);
void test_state_rect_size(void);
void test_state_unit_sphere_volume(void);
void test_state_split(void);
void test_state_node_page(void);
void test_state_identical_null(void);
void test_state_identical_dims_equal(void);
void test_state_identical_dims_unequal(void);
void test_state_bytes_null(void);
void test_state_bytes_non_null(void);
