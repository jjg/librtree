/*
  assert_ps_valid.h

  Checks that a path is valid postscript file by shelling
  out to ghostscript to parse it.

  Copyright (c) J.J. Green 2020
*/

#ifndef ASSERT_PS_VALID_H
#define ASSERT_PS_VALID_H

#define CU_ASSERT_PS_VALID(path) assert_ps_valid(path)

void assert_ps_valid(const char*);

#endif
