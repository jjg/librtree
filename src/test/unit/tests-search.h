#include <CUnit/CUnit.h>

extern CU_TestInfo tests_search[];

void test_search_empty(void);
void test_search_single_square(void);
void test_search_single_line(void);
void test_search_split_horizontal(void);
void test_search_grid_small(void);
void test_search_grid_large(void);
