#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-node.h"

#include <private/branch.h>
#include <private/node.h>
#include <private/state.h>

CU_TestInfo tests_node[] = {
  {"init", test_node_init},
  {"new", test_node_new},
  {"branch each", test_node_branch_each},
  {"add branch", test_node_add_branch},
  {"detach branch", test_node_detach_branch},
  {"envelope, empty", test_node_envelope_empty},
  {"envelope, single", test_node_envelope_single},
  {"envelope, multiple", test_node_envelope_multiple},
  {"add rect, single", test_node_add_rect_single},
  {"add rect, no split", test_node_add_rect_no_split},
  {"add rect, split", test_node_add_rect_split},
  {"clone, empty", test_node_clone_empty},
  {"identical, null", test_node_identical_null},
  {"identical, empty", test_node_identical_empty},
  {"identical, nonempty", test_node_identical_nonempty},
  {"height, empty", test_node_height_empty},
  {"height, single", test_node_height_single},
  {"height, split", test_node_height_split},
  {"bytes, null", test_node_bytes_null},
  {"bytes, single", test_node_bytes_single},
  CU_TEST_INFO_NULL
};

void test_node_init(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *node = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(node);

  int err = node_init(state, node);
  CU_ASSERT_EQUAL(err, 0);

  node_destroy(state, node);
  state_destroy(state);
}

void test_node_new(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *node = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(node);

  node_destroy(state, node);
  state_destroy(state);
}

/* count non-empty branches */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
static int iter_sum(const state_t *state, const branch_t *branch, void *arg)
{
  if (branch_get_child(branch) != NULL)
    {
      int *sum = (int*)arg;
      (*sum)++;
    }

  return 0;
}
#pragma GCC diagnostic pop

void test_node_branch_each(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *root = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(root);

  int sum = 0;
  int err;

  /* on empty node, no branches reported */

  err = node_branch_each(state, root, iter_sum, (void*)&sum);
  CU_ASSERT_EQUAL(err, 0);
  CU_ASSERT_EQUAL(sum, 0);

  rtree_coord_t rect[] = {
    1.0, 2.0,
    2.0, 3.0
  };

  /* add a rectangle */

  CU_ASSERT_PTR_EQUAL(node_add_rect(state, 3, rect, root, 0), root);

  /* which should now be reported */

  err = node_branch_each(state, root, iter_sum, (void*)&sum);
  CU_ASSERT_EQUAL(err, 0);
  CU_ASSERT_EQUAL(sum, 1);

  node_destroy(state, root);
  state_destroy(state);
}

void test_node_add_branch(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *node = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(node);

  size_t n = branch_sizeof(2);
  char buffer[n];
  branch_t *branch = (branch_t*)buffer;
  branch_set_id(branch, 42);

  CU_ASSERT_EQUAL(node_count(node), 0);
  CU_ASSERT_PTR_NOT_NULL(node_add_branch(state, node, branch));
  CU_ASSERT_EQUAL(node_count(node), 1);

  node_destroy(state, node);
  state_destroy(state);
}

void test_node_detach_branch(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *node = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(node);

  size_t n = branch_sizeof(2);
  char buffer[n];
  branch_t *branch = (branch_t*)buffer;

  for (size_t i = 0 ; i < 3 ; i++)
    {
      branch_set_id(branch, 42 + i);
      CU_ASSERT_EQUAL(node_count(node), i);
      CU_ASSERT_PTR_NOT_NULL(node_add_branch(state, node, branch));
    }

  CU_ASSERT_EQUAL(node_count(node), 3);
  CU_ASSERT_NOT_EQUAL(node_detach_branch(state, node, 4), 0);
  CU_ASSERT_EQUAL(node_count(node), 3);

  for (size_t i = 0 ; i < 3 ; i++)
    {
      CU_ASSERT_EQUAL(node_count(node), 3 - i);
      CU_ASSERT_EQUAL(node_detach_branch(state, node, 0), 0);
      CU_ASSERT_EQUAL(node_count(node), 3 - i - 1);
    }

  CU_ASSERT_NOT_EQUAL(node_detach_branch(state, node, 0), 0);
  CU_ASSERT_EQUAL(node_count(node), 0);

  node_destroy(state, node);
  state_destroy(state);
}

/*
  in the case that the node is empty, then the rectangle
  is not modified
*/

void test_node_envelope_empty(void)
{
  size_t dims = 2;
  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *node = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(node);

  rtree_coord_t env[2 * dims];

  for (size_t i = 0 ; i < 2 * dims ; i++)
    env[i] = i;

  CU_ASSERT_EQUAL(node_envelope(state, node, env), 0);

  for (size_t i = 0 ; i < 2 * dims ; i++)
    CU_ASSERT_EQUAL(env[i], (float)i);

  node_destroy(state, node);
  state_destroy(state);
}

/* the envelope of a single rect is that rect */

void test_node_envelope_single(void)
{
  size_t dims = 2;

  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *root = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(root);

  rtree_coord_t rect[] = {1, 2, 2, 3};
  node_t *node = node_add_rect(state, 3, rect, root, 0);
  CU_ASSERT_PTR_EQUAL(node, root);

  rtree_coord_t env[] = {0, 0, 0, 0};
  CU_ASSERT_EQUAL(node_envelope(state, node, env), 0);

  for (size_t i = 0 ; i < 2 * dims ; i++)
    CU_ASSERT_EQUAL(rect[i], env[i]);

  node_destroy(state, root);
  state_destroy(state);
}

/* the envelope of a node with multiple recs */

void test_node_envelope_multiple(void)
{
  size_t dims = 2;

  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *root = node_new(state), *node;
  CU_ASSERT_PTR_NOT_NULL_FATAL(root);

  rtree_coord_t
    rect1[] = {1, 2, 2, 3},
    rect2[] = {4, 5, 5, 6};

  node = node_add_rect(state, 1, rect1, root, 0);
  CU_ASSERT_PTR_EQUAL(node, root);

  node = node_add_rect(state, 2, rect2, root, 0);
  CU_ASSERT_PTR_EQUAL(node, root);

  rtree_coord_t
    expected[] = {1, 2, 5, 6},
    env[] = {0, 0, 0, 0};

  CU_ASSERT_EQUAL(node_envelope(state, node, env), 0);

  for (size_t i = 0 ; i < 2 * dims ; i++)
    CU_ASSERT_EQUAL(expected[i], env[i]);

  node_destroy(state, root);
  state_destroy(state);
}

/* add a single rect, the root node should not split */

void test_node_add_rect_single(void)
{
  size_t dims = 2;
  rtree_coord_t rect[] = {
    1.0, 2.0,
    2.0, 3.0
  };

  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *root = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(root);

  node_t *node = node_add_rect(state, 3, rect, root, 0);

  CU_ASSERT_PTR_NOT_NULL_FATAL(node);
  CU_ASSERT_EQUAL(node, root);

  node_destroy(state, root);
  state_destroy(state);
}

void test_node_add_rect_multiple(void)
{
  size_t dims = 2;
  rtree_coord_t rect[] = {
    1.0, 2.0,
    2.0, 3.0
  };
  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *root = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(root);

  node_t *node = node_add_rect(state, 3, rect, root, 0);

  CU_ASSERT_PTR_NOT_NULL_FATAL(node);
  CU_ASSERT_EQUAL(node, root);

  node_destroy(state, root);
  state_destroy(state);
}

/*
  add enough rects to fill the bottom level, the root node
  should still not split
*/

void test_node_add_rect_no_split(void)
{
  size_t dims = 2;
  rtree_coord_t rect[4] = {0};

  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *root = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(root);

  size_t n = state_branching_factor(state);

  for (size_t i = 0 ; i < n ; i++)
    {
      rect[2] = rect[3] = i;

      node_t *node = node_add_rect(state, i, rect, root, 0);

      CU_ASSERT_PTR_NOT_NULL_FATAL(node);
      CU_ASSERT_EQUAL(node, root);
    }

  node_destroy(state, root);
  state_destroy(state);
}

/*
  add enough rects to overfill the bottom level, the root node
  should now split
*/

void test_node_add_rect_split(void)
{
  size_t dims = 2;
  rtree_coord_t rect[4] = {0};

  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *root = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(root);

  size_t n = state_branching_factor(state);

  for (size_t i = 0 ; i < n ; i++)
    {
      rect[0] = rect[1] = i;
      rect[2] = rect[3] = i + 1;

      node_t *node = node_add_rect(state, i, rect, root, 0);

      CU_ASSERT_PTR_NOT_NULL_FATAL(node);
      CU_ASSERT_EQUAL(node, root);
    }

  rect[2] = rect[3] = n + 1;
  CU_ASSERT_PTR_NOT_NULL(node_add_rect(state, n, rect, root, 0));

  node_destroy(state, root);
  state_destroy(state);
}

void test_node_clone_empty(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *node = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(node);

  node_t *clone = node_clone(state, node);
  CU_ASSERT_PTR_NOT_NULL_FATAL(clone);

  node_destroy(state, clone);
  node_destroy(state, node);
  state_destroy(state);
}

void test_node_identical_null(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *node = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(node);

  CU_ASSERT_TRUE(node_identical(state, NULL, NULL));
  CU_ASSERT_FALSE(node_identical(state, node, NULL));
  CU_ASSERT_FALSE(node_identical(state, NULL, node));

  node_destroy(state, node);
  state_destroy(state);
}

void test_node_identical_empty(void)
{
  state_t *state = state_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *node = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(node);

  node_t *clone = node_clone(state, node);
  CU_ASSERT_PTR_NOT_NULL_FATAL(clone);

  CU_ASSERT_TRUE(node_identical(state, node, node));
  CU_ASSERT_TRUE(node_identical(state, node, clone));

  node_destroy(state, clone);
  node_destroy(state, node);
  state_destroy(state);
}

void test_node_identical_nonempty(void)
{
  size_t dims = 2;

  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *root = node_new(state), *node;
  CU_ASSERT_PTR_NOT_NULL_FATAL(root);

  rtree_coord_t
    rect1[] = {1, 2, 2, 3},
    rect2[] = {4, 5, 5, 6};

  node = node_add_rect(state, 1, rect1, root, 0);
  CU_ASSERT_PTR_EQUAL(node, root);

  node = node_add_rect(state, 2, rect2, root, 0);
  CU_ASSERT_PTR_EQUAL(node, root);

  node_t *clone = node_clone(state, node);
  CU_ASSERT_PTR_NOT_NULL_FATAL(clone);

  CU_ASSERT_TRUE(node_identical(state, node, node));
  CU_ASSERT_TRUE(node_identical(state, node, clone));

  node_destroy(state, clone);
  node_destroy(state, root);
  state_destroy(state);
}

void test_node_height_empty(void)
{
  size_t dims = 2;
  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *root = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(root);

  CU_ASSERT_EQUAL(node_height(state, root), 0);

  node_destroy(state, root);
  state_destroy(state);
}

void test_node_height_single(void)
{
  size_t dims = 2;
  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *node, *root = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(root);

  rtree_coord_t rect[] = {1, 2, 2, 3};

  node = node_add_rect(state, 1, rect, root, 0);
  CU_ASSERT_PTR_EQUAL(node, root);

  CU_ASSERT_EQUAL(node_height(state, root), 1);

  node_destroy(state, root);
  state_destroy(state);
}

void test_node_height_split(void)
{
  size_t dims = 2;
  rtree_coord_t rect[4] = {0};

  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *root = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(root);

  /* build a split tree */

  size_t n = state_branching_factor(state);

  for (size_t i = 0 ; i <= n ; i++)
    {
      rect[0] = rect[1] = i;
      rect[2] = rect[3] = i + 1;

      root = node_add_rect(state, i, rect, root, 0);
      CU_ASSERT_PTR_NOT_NULL_FATAL(root);
    }

  /* the root now has two branches */

  CU_ASSERT_EQUAL(node_count(root), 2);

  /* root has height 2 */

  CU_ASSERT_EQUAL(node_height(state, root), 2);

  /* if we delete a branch, it still has height 2 */

  CU_ASSERT_EQUAL(node_detach_branch(state, root, 1), 0);
  CU_ASSERT_EQUAL(node_height(state, root), 2);

  /* if we delete the other branch, it now has height 0 */

  CU_ASSERT_EQUAL(node_detach_branch(state, root, 0), 0);
  CU_ASSERT_EQUAL(node_height(state, root), 0);

  /* but root level remains 1 */

  CU_ASSERT_EQUAL(node_level(root), 1);

  node_destroy(state, root);
  state_destroy(state);
}

void test_node_bytes_null(void)
{
  CU_ASSERT_EQUAL(node_bytes(NULL, NULL), 0);
}

void test_node_bytes_single(void)
{
  size_t dims = 2;
  state_t *state = state_new(dims, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(state);

  node_t *root = node_new(state);
  CU_ASSERT_PTR_NOT_NULL_FATAL(root);

  size_t bytes = node_bytes(state, root);
  CU_ASSERT_NOT_EQUAL(bytes, 0);

  /* add a rectangle, confirm no split has happened */

  rtree_coord_t rect[] = {1, 2, 2, 3};
  node_t *node = node_add_rect(state, 1, rect, root, 0);
  CU_ASSERT_PTR_EQUAL(node, root);

  /* the bytes should not change */

  CU_ASSERT_EQUAL(bytes, node_bytes(state, root));

  node_destroy(state, root);
  state_destroy(state);
}
