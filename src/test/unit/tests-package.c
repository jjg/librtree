#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-package.h"

#include <rtree/package.h>

CU_TestInfo tests_package[] = {
  {"name", test_package_name},
  {"url", test_package_url},
  {"bugreport", test_package_bugreport},
  CU_TEST_INFO_NULL
};

void test_package_name(void)
{
  CU_ASSERT_STRING_EQUAL(rtree_package_name, "librtree");
}

void test_package_url(void)
{
  CU_ASSERT_STRING_EQUAL(rtree_package_url, "https://gitlab.com/jjg/librtree");
}

void test_package_bugreport(void)
{
  CU_ASSERT_STRING_EQUAL(rtree_package_bugreport, "j.j.green@gmx.co.uk");
}
