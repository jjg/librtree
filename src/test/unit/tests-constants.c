#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-constants.h"

#include <private/constants.h>

#include <math.h>
#include <float.h>

CU_TestInfo tests_constants[] = {
  {"M_PI", test_constants_pi},
  {"M_LOG_PI", test_constants_log_pi},
  CU_TEST_INFO_NULL
};

void test_constants_pi(void)
{
  double actual = 4 * atan(1);
  CU_ASSERT_DOUBLE_EQUAL(M_PI, actual, M_PI * DBL_EPSILON);
}

void test_constants_log_pi(void)
{
  double actual = log(4 * atan(1));
  CU_ASSERT_DOUBLE_EQUAL(M_LOG_PI, actual, M_LOG_PI * DBL_EPSILON);
}
