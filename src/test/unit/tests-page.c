#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-page.h"

#include <private/page.h>

CU_TestInfo tests_page[] = {
  {"size", test_page_size},
  CU_TEST_INFO_NULL
};

void test_page_size(void)
{
  size_t sz = 0;
  int status = page_size(&sz);

  CU_ASSERT_EQUAL(status, 0);
  CU_ASSERT_NOT_EQUAL(sz, 0);
}
