#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <CUnit/CUnit.h>
#include "assert-ps-valid.h"

#include <stdlib.h>

#ifdef GS_PATH

#define CMD_LEN 1024

void assert_ps_valid(const char *path)
{
  char cmd[CMD_LEN];
  int written =
    snprintf(cmd, CMD_LEN,
             "%s -q -sDEVICE=nullpage -dNOPAUSE -dBATCH %s",
             GS_PATH, path);
  CU_ASSERT_FATAL(written < CMD_LEN);
  CU_ASSERT_EQUAL(system(cmd), 0);
}

#else

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
void assert_ps_valid(const char *path)
{
  CU_FAIL("no ghostscript executable found");
}
#pragma GCC diagnostic pop

#endif
