#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <CUnit/CUnit.h>

#include "tests-bindex.h"
#include "tests-branch.h"
#include "tests-branches.h"
#include "tests-bsrt.h"
#include "tests-constants.h"
#include "tests-csv.h"
#include "tests-error.h"
#include "tests-json.h"
#include "tests-node.h"
#include "tests-package.h"
#include "tests-page.h"
#include "tests-postscript.h"
#include "tests-rect.h"
#include "tests-rtree.h"
#include "tests-search.h"
#include "tests-spvol.h"
#include "tests-state.h"

#include "cunit-compat.h"

#define ENTRY(a, b) CU_Suite_Entry((a), NULL, NULL, (b))

static CU_SuiteInfo suites[] =
  {
   ENTRY("bindex", tests_bindex),
   ENTRY("branch", tests_branch),
   ENTRY("branches", tests_branches),
   ENTRY("bsrt", tests_bsrt),
   ENTRY("constants", tests_constants),
   ENTRY("csv", tests_csv),
   ENTRY("error", tests_error),
   ENTRY("json", tests_json),
   ENTRY("node", tests_node),
   ENTRY("package", tests_package),
   ENTRY("page", tests_page),
   ENTRY("postscript", tests_postscript),
   ENTRY("rect", tests_rect),
   ENTRY("rtree", tests_rtree),
   ENTRY("search", tests_search),
   ENTRY("spvol", tests_spvol),
   ENTRY("state", tests_state),
   CU_SUITE_INFO_NULL,
  };

void tests_load(void)
{
  assert(NULL != CU_get_registry());
  assert(!CU_is_test_running());

  if (CU_register_suites(suites) != CUE_SUCCESS)
    {
      fprintf(stderr,"suite registration failed - %s\n",
	      CU_get_error_msg());
      exit(EXIT_FAILURE);
    }
}
