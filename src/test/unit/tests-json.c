#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-json.h"

#include <private/json.h>

#ifdef WITH_JSON

CU_TestInfo tests_json[] = {
  {"write empty", test_json_write_empty},
  {"write non-empty", test_json_write_nonempty},
  {"write zero id", test_json_write_zero_id},
  {"write split square", test_json_write_split_square},
  {"read bad state", test_json_read_bad_state},
  {"read empty", test_json_read_empty},
  {"read non-empty", test_json_read_non_empty},
  {"read split decimated", test_json_read_split_decimated},
  {"read bad leaf", test_json_read_bad_leaf},
  {"read fuzz-01", test_json_read_fuzz01},
  {"read/write", test_json_read_write},
  CU_TEST_INFO_NULL
};

#include "fixture.h"
#include "fixture-json.h"
#include "fixture-csv.h"

#include <jansson.h>

/*
  Tests which compare the output of json_rtree_write() with a reference
  fixture.  We write the JSON to a read-write temporary stream,
  rewind it, then use Jansson to parse both that and the reference
  file, then use Jansson's handy 'json_equal()' to confirm equality.

  Of course, we can only do this with the Jansson library present.
*/

static void assert_json_equal(FILE *stream, const char *file)
{
  long pos = ftell(stream);
  rewind(stream);

  json_t
    *json_written = json_loadf(stream, 0, NULL),
    *json_reference = fixture_json_jansson(file);

  CU_ASSERT_PTR_NOT_NULL_FATAL(json_written);
  CU_ASSERT_PTR_NOT_NULL_FATAL(json_reference);
  CU_ASSERT_EQUAL(json_equal(json_written, json_reference), 1);

  fseek(stream, pos, SEEK_END);

  json_decref(json_reference);
  json_decref(json_written);
}

#define CU_ASSERT_JSON_EQUAL(str, path) assert_json_equal(str, path)

/* write produces expected json in empty case */

void test_json_write_empty(void)
{
  FILE *stream = tmpfile();
  rtree_t *rtree = rtree_new(2, RTREE_NODE_PAGE(1));

  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  CU_ASSERT_EQUAL(json_rtree_write(rtree, stream), 0);
  CU_ASSERT_JSON_EQUAL(stream, "empty.json");

  rtree_destroy(rtree);
  fclose(stream);
}

/* write produces expected json in non-empty case */

void test_json_write_nonempty(void)
{
  FILE *stream = tmpfile();
  rtree_t *rtree = fixture_csv_rtree(2, RTREE_NODE_PAGE(1), "single-square.csv");

  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  CU_ASSERT_EQUAL(json_rtree_write(rtree, stream), 0);
  CU_ASSERT_JSON_EQUAL(stream, "single-square.json");

  rtree_destroy(rtree);
  fclose(stream);
}

/* write produces expected json on id zero */

void test_json_write_zero_id(void)
{
  FILE *stream = tmpfile();
  rtree_t *rtree = fixture_csv_rtree(2, RTREE_NODE_PAGE(1), "zero-id.csv");

  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  CU_ASSERT_EQUAL(json_rtree_write(rtree, stream), 0);
  CU_ASSERT_JSON_EQUAL(stream, "zero-id.json");

  rtree_destroy(rtree);
  fclose(stream);
}

/* write does not error on split */

void test_json_write_split_square(void)
{
  FILE *stream = tmpfile();
  rtree_t *rtree = fixture_csv_rtree(2, 0, "split-square.csv");

  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);
  CU_ASSERT_EQUAL(json_rtree_write(rtree, stream), 0);

  rtree_destroy(rtree);
  fclose(stream);
}

/*
  bad-state.json has a bad page-size state value, so read
  should fail
*/

void test_json_read_bad_state(void)
{
  FILE *stream = fixture_json_stream("bad-state.json");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  errno = 0;
  rtree_t *rtree = json_rtree_read(stream);
  CU_ASSERT_PTR_NULL(rtree);
  CU_ASSERT_EQUAL(errno, EINVAL);

  fclose(stream);
}

void test_json_read_empty(void)
{
  FILE *stream = fixture_json_stream("empty.json");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  rtree_t *rtree = json_rtree_read(stream);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  rtree_destroy(rtree);
  fclose(stream);
}

void test_json_read_non_empty(void)
{
  FILE *stream = fixture_json_stream("single-square.json");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  rtree_t *rtree = json_rtree_read(stream);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  rtree_destroy(rtree);
  fclose(stream);
}

void test_json_read_split_decimated(void)
{
  FILE *stream = fixture_json_stream("split-decimated.json");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  rtree_t *rtree = json_rtree_read(stream);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  rtree_destroy(rtree);
  fclose(stream);
}

void test_json_read_bad_leaf(void)
{
  FILE *stream = fixture_json_stream("bad-leaf.json");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  rtree_t *rtree = json_rtree_read(stream);
  CU_ASSERT_PTR_NULL(rtree);

  fclose(stream);
}

void test_json_read_fuzz01(void)
{
  FILE *stream = fixture_json_stream("afl-01.json");

  if (stream != NULL)
    {
      rtree_t *rtree = json_rtree_read(stream);
      CU_ASSERT_PTR_NULL(rtree);
      fclose(stream);
    }
}

void test_json_read_write(void)
{
  FILE *st1 = fixture_json_stream("split-decimated.json");
  CU_ASSERT_PTR_NOT_NULL_FATAL(st1);

  json_t *json1 = json_loadf(st1, 0, NULL);
  CU_ASSERT_PTR_NOT_NULL_FATAL(json1);
  rewind(st1);

  rtree_t *rtree = json_rtree_read(st1);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  FILE *st2 = tmpfile();
  CU_ASSERT_EQUAL(json_rtree_write(rtree, st2), 0);
  rewind(st2);

  json_t *json2 = json_loadf(st2, 0, NULL);
  CU_ASSERT_PTR_NOT_NULL_FATAL(json2);

  CU_ASSERT_EQUAL(json_equal(json1, json2), 1);

  fclose(st1);
  fclose(st2);

  json_decref(json1);
  json_decref(json2);

  rtree_destroy(rtree);
}

#else

/* WITH_JSON not defined */

CU_TestInfo tests_json[] = {
  {"write no-json", test_json_write_nojson},
  {"read no-json", test_json_read_nojson},
  CU_TEST_INFO_NULL
};

void test_json_write_nojson(void)
{
  FILE *stream = tmpfile();
  rtree_t *rtree = rtree_new(2, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(rtree);

  errno = 0;
  CU_ASSERT_EQUAL(json_rtree_write(rtree, stream), RTREE_ERR_NOJSON);
  CU_ASSERT_EQUAL(errno, ENOSYS);

  fclose(stream);
  rtree_destroy(rtree);
}

void test_json_read_nojson(void)
{
  FILE *stream = tmpfile();
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  errno = 0;
  rtree_t *rtree = json_rtree_read(stream);
  CU_ASSERT_PTR_NULL(rtree);
  CU_ASSERT_EQUAL(errno, ENOSYS);

  fclose(stream);
}

#endif
