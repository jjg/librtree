#include <CUnit/CUnit.h>

extern CU_TestInfo tests_json[];

void test_json_write_empty(void);
void test_json_write_nonempty(void);
void test_json_write_zero_id(void);
void test_json_write_split_square(void);
void test_json_read_bad_state(void);
void test_json_read_empty(void);
void test_json_read_non_empty(void);
void test_json_read_split_decimated(void);
void test_json_read_bad_leaf(void);
void test_json_read_fuzz01(void);
void test_json_read_write(void);

void test_json_write_nojson(void);
void test_json_read_nojson(void);
