#include <CUnit/CUnit.h>

extern CU_TestInfo tests_spvol[];

void test_spvol_zero(void);
void test_spvol_small(void);
void test_spvol_large(void);
