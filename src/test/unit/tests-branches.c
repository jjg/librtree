#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-branches.h"

#include <private/branch.h>
#include <private/branches.h>

#include <rtree/error.h>

#include <stdlib.h>

CU_TestInfo tests_branches[] = {
  {"get/set valid", test_branches_get_set},
  CU_TEST_INFO_NULL
};

void test_branches_get_set(void)
{
  for (size_t dim = 1 ; dim < 10 ; dim++)
    {
      state_t *state = state_new(dim, 0);
      CU_ASSERT_PTR_NOT_NULL_FATAL(state);

      size_t branch_size = state_branch_size(state);

      void *buffer = calloc(2, branch_size);
      CU_ASSERT_PTR_NOT_NULL_FATAL(buffer);

      branch_t *branch = branches_get(state, buffer, 1);
      CU_ASSERT_PTR_NOT_NULL_FATAL(branch);

      CU_ASSERT_EQUAL(branch_init(state, branch), 0);
      branch_set_id(branch, 23);

      branches_set(state, buffer, 0, branch);
      branch = branches_get(state, buffer, 0);
      CU_ASSERT_PTR_NOT_NULL_FATAL(branch);

      CU_ASSERT_EQUAL(branch_get_id(branch), 23);
      free(buffer);
      state_destroy(state);
    }
}
