#include <CUnit/CUnit.h>

extern CU_TestInfo tests_csv[];

void test_csv_read_null_stream(void);
void test_csv_read_short_line(void);
void test_csv_read_simple(void);
void test_csv_read_line_endings(void);

void test_csv_write_null_rtree(void);
void test_csv_write_null_stream(void);
void test_csv_write_empty(void);
void test_csv_write_nonempty(void);
