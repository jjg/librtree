CSV fixtures
------------

These are read by the C unit-test helper `fixture_csv.c`, the format
is standard CSV without header, one unsigned integer (the rectangle
id), and 2 _n_ floats to represent the rectangle.  The helper at present
only handles the _n_ = 2 case.
