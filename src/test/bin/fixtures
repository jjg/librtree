#!/usr/bin/env ruby

# This script generates fixture files in test/fixtures/csv

class Rectangle
  attr_reader :id, :xmin, :xmax, :ymin, :ymax
  PLACES = 6

  def initialize(id, xmin, xmax, ymin, ymax)
    @id = id
    @xmin = xmin
    @xmax = xmax
    @ymin = ymin
    @ymax = ymax
  end

  def row
    [
      id,
      xmin.round(PLACES),
      xmax.round(PLACES),
      ymin.round(PLACES),
      ymax.round(PLACES),
    ]
  end
end

require 'csv'

srand(42)

# random rectangles in the unit square, enough to force a split

CSV.open('fixture/csv/split-square.csv', 'w') do |csv|
  (0..170).map do |id|
    x = [rand, rand].sort
    y = [rand, rand].sort
    rect = Rectangle.new(id, x[0], y[0], x[1], y[1])
    csv << rect.row
  end
end

# disjoint rectangles in a line, easy to split, should give disjoint
# envelopes

CSV.open('fixture/csv/split-horizontal.csv', 'w') do |csv|
  y = [0.0, 1.0]
  (0..170).map do |n|
    x = [n, n + 0.5]
    rect = Rectangle.new(n, x[0], y[0], x[1], y[1])
    csv << rect.row
  end
end

# likewise, but vertical

CSV.open('fixture/csv/split-vertical.csv', 'w') do |csv|
  x = [0.0, 1.0]
  (0..170).map do |n|
    y = [n, n + 0.5]
    rect = Rectangle.new(n, x[0], y[0], x[1], y[1])
    csv << rect.row
  end
end
