#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "fixture-csv.h"
#include "fixture.h"

/* an rtree_t loaded from fixture/csv/<file> */

rtree_t* fixture_csv_rtree(int dim, unsigned flags, const char *file)
{
  size_t n = 1024;
  char path[n];

  if (fixture(path, n, "csv", file) >= (int)n)
    return NULL;

  FILE *stream;

  if ((stream = fopen(path, "r")) != NULL)
    {
      rtree_t *rtree = rtree_csv_read(stream, dim, flags);
      fclose(stream);
      return rtree;
    }

  return NULL;
}
