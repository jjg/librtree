#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "private/bsrt.h"

#include "fixture-bsrt.h"
#include "fixture.h"

#define TPLEN 512

static const char* type_path(const char *file)
{
  static char path[TPLEN];

#if SIZEOF_RTREE_COORD_T == 4
  const char dir[] = "float";
#elif SIZEOF_RTREE_COORD_T == 8
  const char dir[] = "double";
#endif

  if (snprintf(path, TPLEN, "%s/%s", dir, file) >= TPLEN)
    return NULL;

  return path;
}

rtree_t* fixture_bsrt(const char *file)
{
  FILE *stream;

  if ((stream = fixture_bsrt_stream(file)) == NULL)
    return NULL;

  rtree_t *rtree = bsrt_rtree_read(stream);
  fclose(stream);

  return rtree;
}

FILE* fixture_bsrt_stream(const char *file)
{
  size_t n = 1024;
  char path[n];

  if (fixture(path, n, "bsrt", type_path(file)) >= (int)n)
    return NULL;

  return fopen(path, "r");
}
