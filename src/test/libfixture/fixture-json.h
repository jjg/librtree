/*
  fixture_json.h
  Copyright (c) J.J. Green 2019
*/

#ifndef FIXTURE_JSON_H
#define FIXTURE_JSON_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef WITH_JSON

#include <stdio.h>
#include <jansson.h>

json_t* fixture_json_jansson(const char*);
FILE* fixture_json_stream(const char*);

#endif

#endif
