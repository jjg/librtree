/*
  fixture_csv.h
  Copyright (c) J.J. Green 2020
*/

#ifndef FIXTURE_CSV_H
#define FIXTURE_CSV_H

#include "rtree.h"

rtree_t* fixture_csv_rtree(int, unsigned, const char*);

#endif
