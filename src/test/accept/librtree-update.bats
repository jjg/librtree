#!/usr/bin/env bats

load 'config'
load 'shared'

setup()
{
    program='librtree-update'
    path="${src_dir}/embed/${program}"
}

@test 'librtree-update --help' {
    run $path --help
    [ "${status}" -eq 0 ]
}
