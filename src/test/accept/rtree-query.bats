#!/usr/bin/env bats

load 'config'
load 'shared'

setup()
{
    program='rtree-query'
    path="${src_dir}/${program}/${program}"
    build="${src_dir}/rtree-build/rtree-build"
}

@test 'rtree-query, --version' {
    run $path --version
    [ "${status}" -eq 0 ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'rtree-query, --help' {
    run $path --help
}

@test 'rtree-query, --unknown' {
    run $path --unknown
    [ "${status}" -ne 0 ]
}

@test 'rtree-query, --tree json' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/query-json.json"
    result="${BATS_TEST_TMPDIR}/query-json.csv"
    run $build -d 2 -f json -o $json $csv
    run $path -v -t $json -o $result $csv
    [ "${status}" -eq 0 ]
    [ -e "${result}" ]
}

@test 'rtree-query, --tree bsrt' {
    csv="${fixture_dir}/csv/split-square.csv"
    bsrt="${BATS_TEST_TMPDIR}/query-bsrt.bsrt"
    result="${BATS_TEST_TMPDIR}/query-bsrt.csv"
    run $build -d 2 -f bsrt -o $bsrt $csv
    run $path -v -t $bsrt -o $result $csv
    [ "${status}" -eq 0 ]
    [ -e "${result}" ]
}

@test 'rtree-query, no --tree' {
    result="${BATS_TEST_TMPDIR}/query-no-tree.csv"
    run $path -v -o $result $csv
    [ "${status}" -ne 0 ]
    [ ! -e "${result}" ]
}

@test 'rtree-query, read from stdin' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/query-json.json"
    result="${BATS_TEST_TMPDIR}/query-stdin.csv"
    run $build -d 2 -f json -o $json $csv
    run $path -v -t $json -o $result < $csv
    [ "${status}" -eq 0 ]
    [ -e "${result}" ]
}

@test 'rtree-query, read inexistent file' {
    csv1="${fixture_dir}/csv/split-square.csv"
    csv2="${fixture_dir}/csv/no-such-file.csv"
    json="${BATS_TEST_TMPDIR}/query-json.json"
    result="${BATS_TEST_TMPDIR}/query-stdin.csv"
    run $build -d 2 -f json -o $json $csv1
    run $path -v -t $json -o $csv2 $result
    [ "${status}" -ne 0 ]
    [ ! -e "${result}" ]
}

@test 'rtree-query, too many files' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/query-json.json"
    result="${BATS_TEST_TMPDIR}/query-too-many.csv"
    run $build -d 2 -f json -o $json $csv
    run $path -v -t $json -o $result $csv $csv
    [ "${status}" -ne 0 ]
    [ ! -e "${result}" ]
}

@test 'rtree-query, write to stdout' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/query-json.json"
    result="${BATS_TEST_TMPDIR}/query-stdout.csv"
    run $build -d 2 -f json -o $json $csv
    run $path -v -t $json $csv > $result
    [ "${status}" -eq 0 ]
    [ -e "${result}" ]
}

@test 'rtree-query, write inexistent directory' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/query-json.json"
    result="${BATS_TEST_TMPDIR}/no-such-directory/results.csv"
    run $build -d 2 -f json -o $json $csv
    run $path -v -t $json $csv $result
    [ "${status}" -ne 0 ]
    [ ! -e "${result}" ]
}
