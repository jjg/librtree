#!/usr/bin/env bats

load 'config'
load 'shared'

setup()
{
    program='rtree-eps'
    path="${src_dir}/${program}/${program}"
    build="${src_dir}/rtree-build/rtree-build"
}

@test 'rtree-eps, --version' {
    run $path --version
    [ "${status}" -eq 0 ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'rtree-eps, --help' {
    run $path --help
}

@test 'rtree-eps, --list-styles' {
    run $path --list-styles
}

@test 'rtree-eps, --list-styles (verbose)' {
    run $path --list-styles --verbose
}

@test 'rtree-eps, json' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/split-square.json"
    eps="${BATS_TEST_TMPDIR}/eps-read-json.eps"
    run $build -d 2 -f json -s linear -o $json $csv
    run $path -v -f json -o $eps $json
    [ "${status}" -eq 0 ]
    [ -e "${eps}" ]
}

@test 'rtree-eps, bsrt' {
    csv="${fixture_dir}/csv/split-square.csv"
    bsrt="${BATS_TEST_TMPDIR}/split-square.bsrt"
    eps="${BATS_TEST_TMPDIR}/eps-read-bsrt.eps"
    run $build -d 2 -f bsrt -s linear -o $bsrt $csv
    run $path -v -f bsrt -o $eps $bsrt
    [ "${status}" -eq 0 ]
    [ -e "${eps}" ]
}

@test 'rtree-eps, detect format (bsrt)' {
    csv="${fixture_dir}/csv/split-square.csv"
    bsrt="${BATS_TEST_TMPDIR}/split-square.bsrt"
    eps="${BATS_TEST_TMPDIR}/eps-detect-format-bsrt.eps"
    run $build -d 2 -f bsrt -s linear -o $bsrt $csv
    run $path -o $eps $bsrt
    [ "${status}" -eq 0 ]
    [ -e "${eps}" ]
}

@test 'rtree-eps, detect format (json)' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/split-square.json"
    eps="${BATS_TEST_TMPDIR}/eps-detect-format-json.eps"
    run $build -d 2 -f json -s linear -o $json $csv
    run $path -v -o $eps $json
    [ "${status}" -eq 0 ]
    [ -e "${eps}" ]
}

@test 'rtree-eps, --verbose' {
    csv="${fixture_dir}/csv/split-square.csv"
    bsrt="${BATS_TEST_TMPDIR}/split-square.bsrt"
    eps="${BATS_TEST_TMPDIR}/eps-verbose.eps"
    run $build -d 2 -f bsrt -s linear -o $bsrt $csv
    run $path --verbose -f bsrt -o $eps $bsrt
    [ "${status}" -eq 0 ]
    [ -e "${eps}" ]
}

@test 'rtree-eps, --style exists' {
    csv="${fixture_dir}/csv/split-square.csv"
    style="${fixture_dir}/style/valid"
    bsrt="${BATS_TEST_TMPDIR}/split-square.bsrt"
    eps="${BATS_TEST_TMPDIR}/eps-style-exists.eps"
    run $build -d 2 -f bsrt -s linear -o $bsrt $csv
    run $path --style $style -f bsrt -o $eps $bsrt
    [ "${status}" -eq 0 ]
    [ -e "${eps}" ]
}

@test 'rtree-eps, --style does not exist' {
    csv="${fixture_dir}/csv/split-square.csv"
    style="${fixture_dir}/style/no-such-style"
    bsrt="${BATS_TEST_TMPDIR}/split-square.bsrt"
    eps="${BATS_TEST_TMPDIR}/eps-style-does-not-exist.eps"
    run $build -d 2 -f bsrt -s linear -o $bsrt $csv
    run $path --style $style -f bsrt -o $eps $bsrt
    [ "${status}" -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'rtree-eps, --height' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/split-square.json"
    eps="${BATS_TEST_TMPDIR}/eps-height.eps"
    run $build -d 2 -f json -s linear -o $json $csv
    run $path --height 3in -f json -o $eps $json
    [ "${status}" -eq 0 ]
    [ -e "${eps}" ]
}

@test 'rtree-eps, --height bad unit' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/split-square.json"
    eps="${BATS_TEST_TMPDIR}/eps-height-bad-unit.eps"
    run $build -d 2 -f json -s linear -o $json $csv
    run $path --height 3miles -f json -o $eps $json
    [ "${status}" -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'rtree-eps, --width' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/split-square.json"
    eps="${BATS_TEST_TMPDIR}/eps-width.eps"
    run $build -d 2 -f json -s linear -o $json $csv
    run $path --width 3in -f json -o $eps $json
    [ "${status}" -eq 0 ]
    [ -e "${eps}" ]
}

@test 'rtree-eps, --width default unit' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/split-square.json"
    eps="${BATS_TEST_TMPDIR}/eps-width-default-unit.eps"
    run $build -d 2 -f json -s linear -o $json $csv
    run $path --width 3 -f json -o $eps $json
    [ "${status}" -eq 0 ]
    [ -e "${eps}" ]
}

@test 'rtree-eps, --width bad unit' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/split-square.json"
    eps="${BATS_TEST_TMPDIR}/eps-width-bad-unit.eps"
    run $build -d 2 -f json -s linear -o $json $csv
    run $path --width 3miles -f json -o $eps $json
    [ "${status}" -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'rtree-eps, --width and --height' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/split-square.json"
    eps="${BATS_TEST_TMPDIR}/eps-width-and-height.eps"
    run $build -d 2 -f json -s linear -o $json $csv
    run $path --height 3in --width 3in -f json -o $eps $json
    [ "${status}" -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'rtree-eps, --margin' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/split-square.json"
    eps="${BATS_TEST_TMPDIR}/eps-margin.eps"
    run $build -d 2 -f json -s linear -o $json $csv
    run $path --margin 1in -f json -o $eps $json
    [ "${status}" -eq 0 ]
    [ -e "${eps}" ]
}

@test 'rtree-eps, --margin bad unit' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/split-square.json"
    eps="${BATS_TEST_TMPDIR}/eps-margin-bad-unit.eps"
    run $build -d 2 -f json -s linear -o $json $csv
    run $path --margin 1mile -f json -o $eps $json
    [ "${status}" -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'rtree-eps, read stdin' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/split-square.json"
    eps="${BATS_TEST_TMPDIR}/eps-read-stdin.eps"
    run $build -d 2 -f json -s linear -o $json $csv
    run $path -f json -o $eps < $json
    [ "${status}" -eq 0 ]
    [ -e "${eps}" ]
}

@test 'rtree-eps, too many input files' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/split-square.json"
    eps="${BATS_TEST_TMPDIR}/eps-too-many-input-files.eps"
    run $build -d 2 -f json -s linear -o $json $csv
    run $path -f json -o $eps $json $json
    [ "${status}" -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'rtree-eps, write stdout' {
    csv="${fixture_dir}/csv/split-square.csv"
    json="${BATS_TEST_TMPDIR}/split-square.json"
    eps="${BATS_TEST_TMPDIR}/eps-write-stdout.eps"
    run $build -d 2 -f json -s linear -o $json $csv
    run $path -v -f json $json > $eps
    [ "${status}" -eq 0 ]
    [ -e "${eps}" ]
}

@test 'rtree-eps, unknown option' {
    run $path --unknown
    [ "${status}" -ne 0 ]
}

@test 'rtree-eps, bsrt fuzz 01' {
    bsrt="${fixture_dir}/bsrt/float/afl-01.bsrt"
    eps="${BATS_TEST_TMPDIR}/eps-bsrt-fuzz-01.eps"
    run $path -f bsrt -o $eps $bsrt
    [ "${status}" -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'rtree-eps, bsrt fuzz 02' {
    bsrt="${fixture_dir}/bsrt/float/afl-02.bsrt"
    eps="${BATS_TEST_TMPDIR}/eps-bsrt-fuzz-02.eps"
    run $path -f bsrt -o $eps $bsrt
    [ "${status}" -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'rtree-eps, json fuzz 01' {
    json="${fixture_dir}/json/float/afl-01.json"
    eps="${BATS_TEST_TMPDIR}/eps-json-fuzz-01.eps"
    run $path -f json -o $eps $json
    [ "${status}" -ne 0 ]
    [ ! -e "${eps}" ]
}
