#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "units.h"

#include <string.h>

typedef struct
{
  const char *shrt, *lng;
  double scale;
} row_t;

static int unit_scale(const char *unit, double *scale)
{
  static const row_t table[] =
    {
     {"P", "pspt", 1.0},
     {"p", "pt", 0.99626401},
     {"i", "in", 72.0},
     {"m", "mm", 2.83464567},
     {"c", "cm", 28.3464567},
    };
  size_t n = sizeof(table) / sizeof(row_t);

  for (size_t i = 0 ; i < n ; i++)
    {
      row_t row = table[i];
      if ((strcmp(row.shrt, unit) == 0) || (strcmp(row.lng, unit) == 0))
        {
          *scale = row.scale;
          return 0;
        }
    }

  return 1;
}

int units_parse(const char *str, rtree_coord_t *pps)
{
  double val, scale;
  char unit[9];

  switch (sscanf(str, "%lf%8s", &val, unit))
    {
    case 0:
      return 1;
    case 1:
      scale = 1.0;
      break;
    case 2:
      if (unit_scale(unit, &scale) != 0)
        return 1;
      break;
    default:
      return 1;
    }

  *pps = val * scale;

  return 0;
}
