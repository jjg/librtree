/*
  units.h
  parse a double-unit string

  Copyright (c) J.J. Green 2020
*/

#ifndef UNITS_H
#define UNITS_H

#include <rtree.h>

int units_parse(const char*, rtree_coord_t*);

#endif
