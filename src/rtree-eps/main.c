#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include "options.h"
#include "rtree-eps.h"
#include "units.h"

int wrap(struct gengetopt_args_info *info)
{
  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  rtree_eps_t opt;

  if (info->list_styles_given)
    {
      rtree_list_styles(info->verbose_given);
      return EXIT_SUCCESS;
    }

  opt.verbose = info->verbose_given;
  opt.path.eps = (info->output_given ? info->output_arg : NULL);
  opt.style = (info->style_given ? info->style_arg : NULL);

  if (info->height_given)
    {
      if (info->width_given)
        {
          fprintf(stderr, "cannot specify both --width and --height\n");
          return EXIT_FAILURE;
        }
      else
        {
          if (units_parse(info->height_arg, &opt.extent) != 0)
            {
              fprintf(stderr, "failed to parse '%s'\n", info->height_arg);
              return EXIT_FAILURE;
            }
          opt.axis = axis_height;
        }
    }
  else
    {
      if (units_parse(info->width_arg, &opt.extent) != 0)
        {
          fprintf(stderr, "failed to parse '%s'\n", info->width_arg);
          return EXIT_FAILURE;
        }
      opt.axis = axis_width;
    }

  if (units_parse(info->margin_arg, &opt.margin) != 0)
    {
      fprintf(stderr, "failed to parse '%s'\n", info->margin_arg);
      return EXIT_FAILURE;
    }

  switch (info->inputs_num)
    {
    case 0:
      opt.path.tree = NULL;
      break;

    case 1:
      opt.path.tree = info->inputs[0];
      break;

    default:
      fprintf(stderr,"sorry, only one file at a time\n");
      return EXIT_FAILURE;
    }

  switch (info->format_arg)
    {
    case format_arg_json:
      opt.format = format_json;
      break;
    case format_arg_bsrt:
      opt.format = format_bsrt;
      break;
    default:
      opt.format = format_auto;
    }

  if ((opt.path.eps == NULL) && (opt.verbose))
    {
      fprintf(stderr, "output to sdtout, verbosity suppressed\n");
      opt.verbose = false;
    }

  if (opt.verbose)
    printf("This is rtree-eps (version %s)\n", VERSION);

  int err = rtree_eps(&opt);

  if (err)
    fprintf(stderr, "failed\n");
  else if (opt.verbose)
    {
      printf("PostScript plot written to %s\n", opt.path.eps);
      printf("done.\n");
    }

  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;

  if (options(argc, argv, &info) != 0)
    {
      fprintf(stderr, "failed to parse command line\n");
      return EXIT_FAILURE;
    }

  int err = wrap(&info);

  options_free(&info);

  return err;
}
