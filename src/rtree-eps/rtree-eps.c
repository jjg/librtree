#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "rtree-eps.h"

#include <rtree.h>

#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>

#ifndef STYLE_DIR
#define STYLE_DIR "/usr/local/share/rtree"
#endif

typedef struct
{
  const rtree_eps_t *opt;
  const postscript_style_t *style;
  const rtree_t *rtree;
} workspace_t;


static int plot_stream(workspace_t *work, FILE *stream)
{
  rtree_postscript_t opt = {
    .style = work->style,
    .axis = work->opt->axis,
    .extent = work->opt->extent,
    .margin = work->opt->margin,
    .title = "rtree-eps output"
  };

  int err = rtree_postscript(work->rtree, &opt, stream);

  if (err != RTREE_OK)
    fprintf(stderr, "plot failed: %s\n", rtree_strerror(err));

  return err;
}

static int plot(workspace_t *work)
{
  const char *eps_path = work->opt->path.eps;

  if (eps_path == NULL)
    return plot_stream(work, stdout);

  FILE *stream;

  if ((stream = fopen(eps_path, "w")) == NULL)
    {
      fprintf(stderr, "failed to open %s\n", eps_path);
      return 1;
    }

  int err = plot_stream(work, stream);
  fclose(stream);

  return err;
}

#define PLURAL(x) ((x == 1) ? "" : "s")

static int load_rtree_stream(workspace_t *work, FILE *stream)
{
  rtree_t *rtree;
  format_t format = work->opt->format;

  /*
    Auto-detection of input format, we read the first 4 bytes of
    the file, and if it is "BSRt" we use format_bsrt, else we use
    format_json.  This requires us to ftell(3) to get the current
    position in the file and fseek(3) to get back there.  These
    will typically fail on stdin, since that is not seekable.
  */

  if (format == format_auto)
    {
      long start;

      if ((start = ftell(stream)) == -1)
        {
          fprintf(stderr, "failed to get stream position");
          if (errno != 0)
            fprintf(stderr, " (%s)\n", strerror(errno));
          else
            fprintf(stderr, "\n");
          fprintf(stderr, "this happens reading stdin, try --format\n");
          return 1;
        }

      char magic[4];

      if (fread(magic, 1, 4, stream) == 4)
        {
          if (memcmp(magic, "BSRt", 4) == 0)
            format = format_bsrt;
          else
            format = format_json;
        }
      else
        {
          /*
            One could have a legal JSON file of two bytes, "{}",
            although that could not be a legal R-tree JSON, but
            reporting that is the responsibility of the JSON
            reader.
          */

          format = format_json;
        }

      if (fseek(stream, start, SEEK_SET) != 0)
        {
          fprintf(stderr, "failed seek of rtree stream");
          if (errno != 0)
            fprintf(stderr, " (%s)\n", strerror(errno));
          else
            fprintf(stderr, "\n");
          return 1;
        }
    }

  rtree_t* (*reader)(FILE*) = NULL;
  const char *format_name;

  switch (format)
    {
    case format_json:
      reader = rtree_json_read;
      format_name = "JSON";
      break;
    case format_bsrt:
      reader = rtree_bsrt_read;
      format_name = "BSRT";
      break;
    default:
      return 1;
    }

  if ((rtree = reader(stream)) == NULL)
    {
      fprintf(stderr, "failed read of rtree");
      if (errno != 0)
        fprintf(stderr, " (%s)\n", strerror(errno));
      else
        fprintf(stderr, "\n");

      return 1;
    }

  if (work->opt->verbose)
    {
      unsigned
        rh = rtree_height(rtree),
        sh = work->style->n;
      printf("style has %u level%s\n", sh, PLURAL(sh));
      printf("R-tree (from %s) has %u level%s\n",
             format_name, rh, PLURAL(rh));
      if (sh < rh)
        printf("leaves will not be plotted\n");
    }

  work->rtree = rtree;
  int err = plot(work);
  rtree_destroy(rtree);

  return err;
}

static int load_rtree(workspace_t *work)
{
  const char *tree_path = work->opt->path.tree;
  int err = 1;

  if (tree_path)
    {
      FILE *stream;

      if ((stream = fopen(tree_path, "r")) != NULL)
        {
          err = load_rtree_stream(work, stream);
          fclose(stream);
        }
      else
        fprintf(stderr, "failed to open %s\n", tree_path);
    }
  else
    err = load_rtree_stream(work, stdin);

  return err;
}

#define STYLE_LEN 1024

static const char* find_style(const char *base)
{
  static char buffer[STYLE_LEN];

  if (snprintf(buffer, STYLE_LEN, "%s", base) < STYLE_LEN)
    {
      if (access(buffer, R_OK) == 0)
        return buffer;
    }

  if (snprintf(buffer, STYLE_LEN, "%s.style", base) < STYLE_LEN)
    {
      if (access(buffer, R_OK) == 0)
        return buffer;
    }

  if (snprintf(buffer, STYLE_LEN, "%s/%s", STYLE_DIR, base) < STYLE_LEN)
    {
      if (access(buffer, R_OK) == 0)
        return buffer;
    }

  if (snprintf(buffer, STYLE_LEN, "%s/%s.style", STYLE_DIR, base) < STYLE_LEN)
    {
      if (access(buffer, R_OK) == 0)
        return buffer;
    }

  return NULL;
}

static int load_style(workspace_t *work)
{
  int err = 1;

  if (work->opt->style)
    {
      const char *path;

      if ((path = find_style(work->opt->style)) == NULL)
        fprintf(stderr, "could not find style '%s'\n", work->opt->style);
      else
        {
          FILE *stream;

          if ((stream = fopen(path, "r")) == NULL)
            fprintf(stderr, "failed open of %s\n", path);
          else
            {
              postscript_style_t *style =  postscript_style_read(stream);
              fclose(stream);

              if (style == NULL)
                fprintf(stderr, "failed style read from %s\n", path);
              else
                {
                  if (work->opt->verbose)
                    printf("using style %s\n", path);

                  work->style = style;
                  err = load_rtree(work);
                  postscript_style_destroy(style);
                }
            }
        }
    }
  else
    {
      /*
        In case no style was specified, we need a default; we could
        have that as a file and use the same mechanism as above, but
        that adds fragility (what if the package is not yet installed)
        so instead we hard-code the default style.
      */

      static postscript_style_level_t levels[2] = {
        {
         .fill = { .colour = { .model = model_grey, .grey = { 0.9 } } },
         .stroke = { .colour = { .model = model_grey, .grey = { 0.7 } },
                     .width = 1.0 }
        },
        {
         .fill = { .colour = { .model = model_grey, .grey = { 0.5 } } },
         .stroke = { .colour = { .model = model_grey, .grey = { 0.3 } },
                     .width = 0.5 }
        }
      };

      static const postscript_style_t style_default = {
        .n = 2,
        .array = levels
      };

      work->style = &style_default;

      if (work->opt->verbose)
        printf("using the default style\n");

      err = load_rtree(work);
    }

  return err;
}

int rtree_eps(rtree_eps_t *opt)
{
  workspace_t work = { .opt = opt };
  return load_style(&work);
}

/*
  It's nicer to have the styles listed alphabetically, but since
  we read them from a readdir() we don't know how many there are
  when we start reading; we could count-rewind-allocate-read, but
  that's a race condition, so instead we recursively generate a
  linked list on the stack, then at the top sort and print.
*/

typedef struct base_node_t base_node_t;

struct base_node_t
{
  char base[32];
  base_node_t *prev;
};

static int strcmp_vp(const void *a, const void *b)
{
  return strcmp(*(const char**)a, *(const char**)b);
}

static int list_styles(DIR *dir, base_node_t *prev, bool verbose)
{
  struct dirent *entry;
  errno = 0;

  if ((entry = readdir(dir)) != NULL)
    {
      base_node_t node = { .prev = prev };
      char ext[17];

      if ((sscanf(entry->d_name, "%32[^.].%16[^.]", node.base, ext) == 2) &&
          (strcmp(ext, "style") == 0))
        return list_styles(dir, &node, verbose);

      return list_styles(dir, prev, verbose);
    }

  if (errno != 0)
    {
      fprintf(stderr, "error: %s\n", strerror(errno));
      return 1;
    }

  size_t n = 0;
  for (base_node_t *node = prev ; node ; node = node->prev, n++);

  if (n > 0)
    {
      char *index[n];
      base_node_t *node = prev;

      for (size_t i = 0 ; i < n ; i++)
        {
          index[i] = node->base;
          node = node->prev;
        }

      qsort(index, n, sizeof(char*), strcmp_vp);

      const char *prefix = (verbose ? "- " : "");

      for (size_t i = 0 ; i < n ; i++)
        printf("%s%s\n", prefix, index[i]);
    }

  return 0;
}

int rtree_list_styles(bool verbose)
{
  if (verbose)
    printf("styles in %s\n", STYLE_DIR);

  DIR *dir;

  if ((dir = opendir(STYLE_DIR)) == NULL)
    {
      fprintf(stderr, "failed to open directory %s\n", STYLE_DIR);
      return 1;
    }

  if (list_styles(dir, NULL, verbose) != 0)
    {
      fprintf(stderr, "failed style list\n");
      return 1;
    }

  closedir(dir);

  if (verbose)
    printf("done.\n");

  return 0;
}
