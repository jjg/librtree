Documentation
-------------

The library documentation is in Unix manual pages, generated from
[DocBook 5][1] sources using the `xsltproc` program included with
[libxslt][2].  Pre-built manual pages are committed to the Git
repository and included in packages, so `xsltproc` is only needed
to modify the sources.

The same files can be processed to generate HTML versions of the
documentation, these are available on the project's [homepage][3].

[1]: https://docbook.org/
[2]: http://www.xmlsoft.org/XSLT/
[3]: https://jjg.gitlab.io/en/code/librtree/
