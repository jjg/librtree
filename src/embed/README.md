Embedding librtree
------------------

Since **librtree** is rather small (3 KLoC), it may make sense
to embed the library rather than make it a dependency.  The
script here, `librtree-update`, is intended to make this a bit
easier.  It

- clones a copy of the specified branch (into `/tmp`)
- updates itself
- copies the C source and headers and files to be included by
  your `Makefile` to a specified target location.

This is not plug-and-play, there is some initial work to do. But
subsequent updates _should_ be easy.


### The `librtree-update` script

Copy the script to somewhere in your project, typically into a
location for maintenance executables, `bin/libtree-update` for
example.  Make sure it is executable and add it to version
control.


### Execution

You will need some mechanism to execute the script with specific
arguments, the obvious way to do this would be a bottom-level
Makefile -- add the targets

``` make
update-librtree-self:
        bin/librtree-update --verbose --self

update-librtree:
        bin/librtree-update --verbose --target src/librtree

update-librtree-clean:
        bin/librtree-update --verbose --clean
```

Alternatively, the commands could be in wrapper-scripts.

It is best to keep these commands separate, the first updates the
script itself to the latest version, the second will actually copy
the **librtree** source to the directory specified by the `--target`
(and that need not exist, it will be created).  The final command
deletes the cache.

The update procedure (in the Makefile case) is then just

``` sh
make update-librtree-self
make update-librtree
make update-librtree-clean
```

### Configuring the library

The embedded code so-far in incomplete, it lacks

- the file `src/librtree/rtree/types.h` which needs to define the
  types `rtree_id_t` and `rtree_coord_t` (used for the index of the
  rectangles and their extent, respectively) and

- macros `SIZEOF_RTREE_ID_T` and `SIZEOF_RTREE_COORD_T` which give
  the sizes (in bytes) for these types.

So that's easy, you just have a header which defines those types via
`typedef` and the macros in terms of the `sizeof` of types right? Sadly
that doesn't work, the macros are used in pre-processor conditionals
and those are evaluated before the C is parsed, so types are not yet
known and so neither is their size.

So we need to fall back to setting these values at the configuration
stage, rather than the compile.  My preferred configuration tool is
**autoconf** (old-school, I know), so I describe how to do this with
autoconf here, no doubt other configuration tools can do similarly.

For autoconf, edit your bottom-level `configure.ac` and add

``` sh
RTREE_ID_TYPE=uint64_t
RTREE_COORD_TYPE=float

AC_CHECK_SIZEOF([rtree_id_t], [],
  [
    AC_INCLUDES_DEFAULT
    typedef $RTREE_ID_TYPE rtree_id_t;
  ]
)
AC_CHECK_SIZEOF([rtree_coord_t], [],
  [
    AC_INCLUDES_DEFAULT
    typedef $RTREE_COORD_TYPE rtree_coord_t;
  ]
)

AC_SUBST([RTREE_ID_TYPE])
AC_SUBST([RTREE_COORD_TYPE])

AC_CONFIG_FILES([src/librtree/rtree/types.h])
AC_CONFIG_HEADERS([src/librtree/config.h])
```

This defines the types, calculates their sizes and adds the types
to the variables for substitution.  It then instructs autoconf to
generate `types.h` from `types.h.in`.  Manually create the latter
to contain the type definitions from those variables:

```
#ifndef RTREE_TYPES_H
#define RTREE_TYPES_H

#include <stdint.h>

typedef @RTREE_COORD_TYPE@ rtree_coord_t;
typedef @RTREE_ID_TYPE@ rtree_id_t;

#endif
```

Now run

``` sh
autoheader
autoconf
./configure
```

and check that `types.h` is created and contains the expected types.
Similarly, check your `config.h` for the expected sizes of the types,
typically

``` c
/* The size of `rtree_coord_t', as computed by sizeof. */
#define SIZEOF_RTREE_COORD_T 4

/* The size of `rtree_id_t', as computed by sizeof. */
#define SIZEOF_RTREE_ID_T 8
```

Don't forget to set `types.h` and `config.h` to be ignored by your
version-control system.  With **git** add

``` sh
/src/librtree/rtree/types.h
/src/librtree/config.h
```
to your `.gitignore`.


### Makefile

Finally, we need a `Makefile` in the `librtree` directory to create
the library.  This is somewhat simplified by files included in the
embedding in the `mk` subdirectory.  So something like

``` make
NAME = rtree

default: all

include mk/Obj.mk
include ../Common.mk

CFLAGS += -I. -I../include
LIB = lib$(NAME).a

RUBBISH += $(LIB) $(OBJ)
CONF = rtree/types.h config.h

all: $(LIB)

$(LIB): $(OBJ)
        $(AR) rs $(LIB) $(OBJ)

clean:
        $(RM) $(RUBBISH)

spotless: clean
        $(RM) $(CONF)

.PHONY: default all clean spotless
```

The included `mk/Obj.mk` defines the `$(OBJ)` variable which lists
the object files needed for the library.


### Notes

This has been written to embed the library in other projects of
mine (see for example [vfplot][1]), so rather follows my preferred
methods and tool-set.  I am open to pull-requests which implement
other build mechanisms.


[1]: https://gitlab.com/jjg/vfplot
