/*
  rtree-build.h
  Copyright (c) J.J. Green 2020
*/

#ifndef RTREE_BUILD_H
#define RTREE_BUILD_H

#include <stdbool.h>
#include <stdlib.h>

typedef enum
  {
   format_unspecified,
   format_json,
   format_bsrt
  } output_format_t;

typedef enum
  {
   split_unspecified,
   split_linear,
   split_quadratic,
   split_greene
  } split_strategy_t;

typedef struct
{
  bool verbose;
  size_t dimension, node_page;
  const char *infile, *outfile;
  output_format_t format;
  split_strategy_t split;
} rtree_build_t;

int rtree_build(rtree_build_t*);

#endif
