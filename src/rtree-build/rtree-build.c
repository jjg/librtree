#include <rtree.h>

#include <errno.h>
#include <string.h>

#include "rtree-build.h"

int rtree_build(rtree_build_t *opt)
{
  rtree_t *rtree = NULL;
  state_flags_t split_flags = 0;

  switch (opt->split)
    {
    case split_linear:
      split_flags = RTREE_SPLIT_LINEAR;
      break;
    case split_quadratic:
      split_flags = RTREE_SPLIT_QUADRATIC;
      break;
    case split_greene:
      split_flags = RTREE_SPLIT_GREENE;
      break;
    case split_unspecified:
      split_flags = RTREE_SPLIT_QUADRATIC;
      break;
    default:
      return 1;
    }

  if (opt->verbose)
    {
      const char *type;

      switch (split_flags)
        {
        case RTREE_SPLIT_LINEAR: type = "linear"; break;
        case RTREE_SPLIT_QUADRATIC: type = "quadratic"; break;
        case RTREE_SPLIT_GREENE: type = "Greene"; break;
        default:
          return 1;
        }

      printf("splitting with %s strategy\n", type);
    }

  int (*writer)(const rtree_t*, FILE*) = NULL;

  switch (opt->format)
    {
    case format_json:
      writer = rtree_json_write;
      break;
    case format_bsrt:
      writer = rtree_bsrt_write;
      break;
    default:
      return 1;
    }

  state_flags_t node_page_flags = RTREE_NODE_PAGE(opt->node_page);
  state_flags_t flags = split_flags | node_page_flags;
  errno = 0;

  if (opt->infile == NULL)
    rtree = rtree_csv_read(stdin, opt->dimension, flags);
  else
    {
      FILE *stream;

      if ((stream = fopen(opt->infile, "r")) == NULL)
        fprintf(stderr, "failed to open %s\n", opt->infile);
      else
        {
          rtree = rtree_csv_read(stream, opt->dimension, flags);
          fclose(stream);
        }
    }

  if (rtree == NULL)
    {
      if (errno != 0)
        fprintf(stderr, "error: %s\n", strerror(errno));
      return 1;
    }

  if (opt->verbose)
    {
      printf("built R-tree with\n");
      printf("- dimension %zi\n", rtree_dims(rtree));
      printf("- page-size %zi\n", rtree_page_size(rtree));
      printf("- node-size %zi\n", rtree_node_size(rtree));
      printf("- branching %zi\n", rtree_branching_factor(rtree));
    }

  int err = -1;

  if (opt->outfile == NULL)
    err = writer(rtree, stdout);
  else
    {
      FILE *stream;

      if ((stream = fopen(opt->outfile, "w")) == NULL)
        fprintf(stderr, "failed to open %s\n", opt->outfile);
      else
        {
          err = writer(rtree, stream);
          fclose(stream);
        }
    }

  rtree_destroy(rtree);

  if (err > 0)
    fprintf(stderr, "error: %s\n", rtree_strerror(err));

  return err ? 1 : 0;
}
