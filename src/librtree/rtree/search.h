/*
  rtree/search.h
  Copyright (c) J.J. Green 2020
*/

#ifndef RTREE_SEARCH_H
#define RTREE_SEARCH_H

#ifdef __cplusplus
extern "C" {
#endif

#include <rtree/types.h>

typedef int (rtree_search_t)(rtree_id_t, void*);

#ifdef __cplusplus
}
#endif

#endif
