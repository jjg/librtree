/*
  private/csv.h
  Copyright (c) J.J. Green 2020
*/

#ifndef PRIVATE_CSV_H
#define PRIVATE_CSV_H

#include <rtree.h>
#include <rtree/state.h>

#include <stdio.h>

int csv_rtree_write(const rtree_t*, FILE*);
rtree_t* csv_rtree_read(FILE*, size_t, state_flags_t);

#endif
