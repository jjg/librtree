/*
  private/bsrt.h
  Copyright (c) J.J. Green 2019
*/

#ifndef PRIVATE_BSRT_H
#define PRIVATE_BSRT_H

#include <private/rtree.h>

#include <stdio.h>

int bsrt_rtree_write(const rtree_t*, FILE*);
rtree_t* bsrt_rtree_read(FILE*);

#endif
