/*
  private/search.h
  Copyright (c) J.J. Green 2023
*/

#ifndef PRIVATE_SEARCH_H
#define PRIVATE_SEARCH_H

#include <private/node.h>
#include <private/state.h>
#include <rtree/search.h>
#include <rtree/types.h>

int search(const state_t*,
           const rtree_coord_t*,
           const node_t*,
           rtree_search_t*,
           void*);

#endif
