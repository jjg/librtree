#!/bin/sh
#
# creates a preview plot of a style file, needs the library
# to be installed, and also the Generic Mapping Tools (GMT)
# for psconvert, superior eps rasterisation

set -e

if [ $# != 3 ]
then
    echo 'usage: preview <style> <csv> <png>'
    exit 1
fi

STYLE="$1"
CSV="$2"
PNG="$3"

BSRT=$(mktemp --suffix .bsrt)
rtree-build -d2 -v -f bsrt -o "$BSRT" "$CSV"

EPS=$(mktemp --suffix .ps)
rtree-eps -W 4i -m 2m -v -s "$STYLE" -o "$EPS" "$BSRT"

gmt psconvert "$EPS" -TG -E100
PNGSRC="$(dirname "$EPS")/$(basename "$EPS" .ps).png"
cp "$PNGSRC" "$PNG"

rm "$BSRT"
rm "$EPS"
rm "$PNGSRC"
