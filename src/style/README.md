R-tree style files
------------------

These are simple JSON files, chosen since the library already has
a dependency on the Jannson library, but we find it more convenient
to write these as YAML (in `src`), then convert to JSON using the
trivial Ruby script `bin/yaml2json`.

The style format should validate against the schema `lib/schema.json`
and the script `bin/stylevalidate` will check this.  The script is in
Ruby and requires that the [json-schemer][1] Gem be available.

[1]: https://github.com/davishmcclurg/json_schemer
